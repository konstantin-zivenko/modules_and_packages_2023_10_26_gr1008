from datetime import datetime


s = "If Comrade Napoleon says it, it must be right."
a = [100, 200, 300]

_a = "single dunder"

__b = "double dunder"


def foo(arg):
    print(f"agr = {arg}")


class Foo:
    pass


datetime_now = datetime.now()


print(__name__)

if __name__ == "__main__":
    print(s)
    print(a)
    foo(s)
